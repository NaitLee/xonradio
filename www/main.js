// @license magnet:?xt=urn:btih:90dc5c0be029de84e523b9b3922520e79e0e6f08&dn=cc0.txt CC0-1.0

'use strict';

// https://gildas-lormeau.github.io/zip.js/
import { BlobReader, ZipReader } from "https://esm.sh/@zip.js/zip.js@2.7.32?exports=BlobReader,ZipReader&bundle-deps";

// https://github.com/adamhaile/S
import S from "https://esm.sh/s-js@0.4.9?bundle-deps";

const Q = (selector) => document.querySelector(selector);
const A = (selector) => document.querySelectorAll(selector);
const E = (tag) => document.createElement(tag);

const show = (selector) => Q(selector).classList.remove('hidden');
const hide = (selector) => Q(selector).classList.add('hidden');

function fmttime(seconds) {
    const mins = (seconds / 60 | 0).toString().padStart(2, '0');
    const secs = (seconds % 60 | 0).toString().padStart(2, '0');
    return mins + ':' + secs;
}


const video = Q('.live__video');

fetch('/~cfg').then(r => r.text()).then(cfg => Q('.xoncfg').innerText = cfg);

fetch('/~about').then(r => r.json()).then(dict => {
    const about = Q('.about');
    for (const name in dict) {
        const a = E('a');
        a.href = dict[name];
        a.target = '_blank';
        a.innerText = name;
        about.appendChild(a);
        const span = E('span');
        span.innerText = ' ';
        about.appendChild(span);
    }
});

fetch('/~list').then(r => r.ok ? r.text() : null).then(list => {
    const browser = Q('.browser');
    if (list === null) {
        browser.innerText = 'Browsing not allowed';
        return;
    }
    if (list.trim() === '') {
        browser.innerText = 'No songs available';
        return;
    }
    for (const line of list.split('\n').map(l => l.trim())) {
        if (line === '') continue;
        const segs = line.split(' ');
        const a = E('a');
        a.href = segs[0];
        const duration = fmttime(parseFloat(segs[2]));
        const oggtitle = segs.slice(3).join(' ');
        a.innerText = `[${duration}] ${oggtitle}`;
        a.download = oggtitle;
        a.addEventListener('click', (event) => {
            event.preventDefault();
            play([segs[0], segs[1], segs[2], oggtitle], 0);
            show('.button-back-to-live');
        });
        browser.appendChild(a);
    }
});

Q('.button-manual-play').addEventListener('click', () => {
    video.play();
    hide('.button-manual-play');
});


const [title, link_ogg, link_pk3] = ['', '', ''].map(x => S.data(x));

[
    () => Q('.link-ogg').href = video.src = link_ogg(),
    () => Q('.link-pk3').href = link_pk3(),
    () => {
        Q('.title').innerText = title();
        Q('.link-ogg').download = title() + '.ogg';
        Q('.link-pk3').download = title() + '.pk3';
    }
].map(S);

async function play(entry, seekto) {
    const [pk3path, oggpath, oggtitle] = [entry[0], entry[1], entry[3]];

    show('.label-loading');
    title(oggtitle);
    link_pk3(pk3path);

    const pk3 = await fetch(pk3path).then(r => r.blob());

    const blob_reader = new BlobReader(pk3);
    const zip_reader = new ZipReader(blob_reader);

    const ogg_chunks = [];

    for await (const entry of zip_reader.getEntriesGenerator()) {
        if (entry.filename !== oggpath) continue;
        const writer = new WritableStream({
            start: () => void 0,
            write: (data) => ogg_chunks.push(data)
        });
        await entry.getData(writer);
        break;
    }
    const ogg = new Blob(ogg_chunks);
    if (link_ogg() !== 'javascript:')
        URL.revokeObjectURL(link_ogg());
    link_ogg(URL.createObjectURL(ogg));

    hide('.label-loading');
    video.currentTime = seekto;
    video.play();
    if (video.paused)
        show('.button-manual-play');
}

async function fill() {
    hide('.button-back-to-live');
    const info = await fetch('/~info').then(r => r.json());
    console.log(info)
    await play(info.current, info.elapsed);
    const t = Q('.title-next');
    t.innerText = info.next[3];
    t.onclick = () => {
        play(info.next, 0);
        show('.button-back-to-live');
    }
}

video.addEventListener('ended', fill);
Q('.button-back-to-live').addEventListener('click', fill);

fill();

// @license-end
