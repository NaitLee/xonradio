# xonradio

A neat Xonotic (SMB) radio host solution in Python. Web goodies included. (Unofficial)

## Usage

The file `config.default.json` contains all the possible configurations and their defaults.  
You may override them by *creating your own config file* `config.json`.

[See introduction for all config keys](./config.md)

You can have a directory containing songs in ogg format, and/or list of pk3s from external source.  
Add them to your `config.json` like:

```json
{
    "lists": [
        "list.txt"
    ],
    "directories": [
        "ogg/"
    ]
}
```

In case you don’t yet have songs in ogg format — you may choose some GUI tools to help you to convert, or:  
- create (or symlink to) a directory `any-audio`, put songs there,  
- then run `python3 convert.py` to have them converted to `ogg` directory,  
- then the `ogg` directory can be used directly.  
(In order for `convert.py` to work, you also need ffmpeg installed.)

And then run `python3 serve.py`, have a look on your service in the browser.

You can find configuration in the web frontpage, and add them to your Xonotic server config. Enjoy!

If you need a systemd service unit, see [xonradio.service](./xonradio.service) for an example.

## Notes

You need to use [SMB Modpack](https://github.com/MarioSMB/modpack) to have radio available *for your server*.

In order to read ogg audio duration (needed by SMB radio to work), you need to:
- preserve the `mutagen` directory in this repository (`pip3 install mutagen` is ok too),
- *or* have `ffmpeg` (needs its `ffprobe`) on your system.

By default, when ogg duration is being read, it is cached and saved to `duration.txt`, and being read again next startup.

The `serve.py` does everything for you, including packaging ogg to pk3 on demand.  
New ogg files in your directories & new entries in `list.txt` can be filled **without** requiring to restart `serve.py`.

## Copyright

The `mutagen` directory contains [Mutagen](https://github.com/quodlibet/mutagen) project code “vendored” to have non-ogg/vorbis parts removed.  
It is licensed under GNU GPL v2 or later. See [`mutagen/COPYING`](./mutagen/COPYING).

(Note that mutagen is optional. Without mutagen, the program takes ogg duration from ffprobe output — and ffmpeg is a dedicated program.)

Inside `www/image/` there are images derived from Xonotic game assets.

Everything else have their associated rights waived with [CC0 1.0](./LICENSE).  
Wondering about GPL compliance? See [this section of GPL FAQ](https://www.gnu.org/licenses/gpl-faq.html#CombinePublicDomainWithGPL).
