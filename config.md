
# Configuration

Create your own config file `config.json` to override defaults in `config.default.json`.

## Keys

### `host: ""`

Which address to listen to. Using blank is ok for most cases.

### `port: 8293`

Which port to use.

### `forbid_non_xonotic: false`

Whether to forbid non-Xonotic clients (e.g. browsers, curl, wget, etc.) to interact with this service.

### `log: true`

Whether log API interactions. You may disable it if you mind it would spam your `journalctl`.

### `lists: []`

Other Xonotic SMB radio solutions generates bunch of pk3s along with a list containing metadata. Entries in such list are for `autofill`.

If you have ever used those services and have such lists, just add them directly to here.  
(Be sure to keep those pk3s served. A static server is enough.)

For the format of pk3 lists, it’s lines of:
```
[http://url/to%20some.pk3] [ogg-path-inside-pk3.ogg] [audio-duration-in-seconds] [Song Title]
```

### `directories: []`

Directories for all of your ogg audio files. Be sure they are in Vorbis codec.

They will be packaged as pk3 & sent to clients on demand.

### `fill_random: false`

Whether to fill a random entry (rather than sequential). Note that filled entry may duplicate, especially when entries are few.

It is observed that in rare cases the random entry may “not work” (previous track played again) due to likely pk3 loading priority.

### `allow_browse: true`

The web interface contains a browser, for visitors to view & order from all available entries (for themselves to listen). Plus an endpoint for getting a full list (`/~list`).

If you don't want that to happen, disable this.

### `cache_duration: true`

After obtaining any ogg file’s duration, it is cached. You may disable it for any possible reason.

You may also want to remove the file `duration.txt` to clear cache. After that, restart the server.

### `web_interface: true`

If you don’t want your service to be publicly “browsable”, disable this.  
You may also want to disable `allow_browse` at the same time.

Note that critical endpoints are still available, such as `/~fill`, `/~pk3` and `/~cfg`.

### `advance_at: 0.4`

There’s a cvar in SMB mod: `sv_radio_queue_advanceat`, introduced [here](https://github.com/MarioSMB/modpack/blob/f0522dbd3aead8f8fc5769ab5ef57e1dddf5b19f/mod/common/radio/sv_radio.qc#L23).

By default it’s `0.4`. This is used for, when current track is played for (duration * advance_at) seconds, pre-fill next track.

This is used for the server for filling next entry & calculating time *correctly*.

If you have changed this cvar in your server config, also change it here.

### `about: {}`

“About” links shown in web frontpage, from for the endpoint `/~about`.

You may use this to promote your other services. Or just clear it if you wish.
