#!/usr/bin/env python3

import os
import io
import random
import json
import zipfile
import urllib
from time import time
from subprocess import check_output as subprocess_check_output
from http.server import BaseHTTPRequestHandler, HTTPServer
try:
    from mutagen.oggvorbis import OggVorbis
except ImportError:
    OggVorbis = None
    for bindir in os.environ['PATH'].split(os.pathsep):
        if os.path.isfile(os.path.join(bindir, 'ffprobe')) or os.path.isfile(os.path.join(bindir, 'ffprobe.exe')):
            print("Note: will use ffprobe to get audio duration, expecting slower performance")
            break
    else:
        print("Warning: no mutagen and no ffprobe, unable to obtain audio duration")

ROUTE_FILL = '/~fill'
ROUTE_BROWSE = '/~browse'
ROUTE_LIST = '/~list'
ROUTE_OGG = '/~ogg/'
ROUTE_PK3 = '/~pk3/'
ROUTE_CFG = '/~cfg'
ROUTE_INFO = '/~info'
ROUTE_ABOUT = '/~about'
WEB_ROOT = 'www'

CFGDEF = 'config.default.json'
CFGFILE = 'config.json'
DURATION_CACHE = 'duration.txt'
SONG_IN_PK3 = 'song.ogg'  # Not recommended to change
INFO_IN_PK3 = 'INFO.txt'

BUFSIZE = 1 << 20

Mime = {
    '.txt': 'text/plain;charset=utf-8',
    '.html': 'text/html;charset=utf-8',
    '.css': 'text/css;charset=utf-8',
    '.js': 'text/javascript;charset=utf-8',
    '.mjs': 'text/javascript;charset=utf-8',
    '.ico': 'image/vnd.microsoft.icon',
    '.zip': 'application/zip',
    '.pk3': 'application/zip',
    '.ogg': 'audio/ogg',
}

def mime(path: str) -> str:
    return Mime.get(os.path.splitext(path)[1], 'application/octet-stream')

def readconfig():
    with open(CFGDEF, 'r', encoding='utf-8') as file:
        config = json.load(file)
    with open(CFGFILE, 'r', encoding='utf-8') as file:
        override = json.load(file)
        for key in override:
            config[key] = override[key]
    return config

Config = readconfig()

DEF_HOST = f"{Config['host']}:{Config['port']}"

class Filler():

    host: str
    ogg_duration_map: dict
    entry: (str, str, str, str)
    next_entry: (str, str, str, str)
    time: float
    duration: float
    advance_at: float

    def __init__(self):
        self.count_lists = len(Config['lists'])
        self.count_directories = len(Config['directories'])
        self.count = self.count_lists + self.count_directories

        self.seqindex = 0
        self.seqmap = {}
        for key in Config['lists']:
            self.seqmap[key] = 0
        for key in Config['directories']:
            self.seqmap[key] = 0

        self.ogg_duration_map = {}
        if os.path.isfile(DURATION_CACHE):
            self.read_duration()
        else:
            with open(DURATION_CACHE, 'wb'):
                pass

        self.use_host(DEF_HOST)
        self.entry = None
        self.next_entry = None
        self.time = time()
        self.duration = 0
        self.advance_at = 0

    def use_host(self, host: str):
        self.host = host

    def duration_of(self, path: str) -> str:
        if path in self.ogg_duration_map:
            duration = self.ogg_duration_map[path]
        else:
            if OggVorbis is None:
                # https://stackoverflow.com/questions/30977472/python-getting-duration-of-a-video-with-ffprobe
                duration = str(round(float(
                    subprocess_check_output(
                        ['ffprobe', '-i', path, '-show_entries', 'format=duration', '-v', 'quiet', '-of', 'csv=p=0']
                    ).decode('utf-8').strip()
                ), 3))
            else:
                duration = str(round(OggVorbis(path).info.length, 3))
            self.ogg_duration_map[path] = duration
            self.save_duration(path, duration)
        return duration

    def path_to_entry(self, path: str) -> (str, str, str, str):
        url = 'http://' + self.host + ROUTE_PK3 + urllib.parse.quote(path + '.pk3')
        title = os.path.splitext(os.path.basename(path))[0]
        duration = self.duration_of(path)
        return (url, SONG_IN_PK3, duration, title)

    def entry_to_line(self, entry: (str, str, str, str)) -> str:
        return ' '.join(entry)

    def _fill_random(self) -> (str, str, str, str):
        i = random.randrange(0, self.count)
        # from directory
        if i >= self.count_lists:
            i -= self.count_lists
            directory = Config['directories'][i]
            path = directory + random.choice(os.listdir(directory))
            return self.path_to_entry(path)
        # from text file
        with open(Config['lists'][i], 'r', encoding='utf-8') as f:
            line = random.choice(f.readlines())
            segs = line.split(' ')
            return (segs[0], segs[1], segs[2], ' '.join(segs[3:]))

    def _fill_seq(self) -> (str, str, str, str):
        i = self.seqindex
        def update_seq(key, lists) -> int:
            if self.seqmap[key] >= len(lists):
                self.seqmap[key] = 0
                self.seqindex += 1
                if self.seqindex >= self.count:
                    self.seqindex = 0
            index = self.seqmap[key]
            self.seqmap[key] += 1
            return index
        if i >= self.count_lists:
            i -= self.count_lists
            # from directory
            directory = Config['directories'][i]
            lists = os.listdir(directory)
            index = update_seq(directory, lists)
            path = directory + lists[index]
            return self.path_to_entry(path)
        else:
            # from text file
            file = Config['lists'][i]
            with open(file, 'r', encoding='utf-8') as f:
                lines = f.readlines()
            index = update_seq(file, lines)
            line = lines[index]
            segs = line.split(' ')
            return (segs[0], segs[1], segs[2], ' '.join(segs[3:]))

    def fill(self) -> str:
        def get():
            if Config['fill_random']:
                return self._fill_random()
            else:
                return self._fill_seq()
        now = time()
        def update(entry):
            self.duration = float(entry[2])
            self.advance_at = self.duration * Config['advance_at'] - 1
            self.time = now
        entry = None
        if self.entry is None:
            entry = self.entry = get()
            update(entry)
        else:
            if self.time + self.advance_at < now:   # necessary to fill the next
                if self.time + self.duration < now: # current entry is end, fill next
                    self.entry = self.next_entry
                    entry = self.entry
                    self.next_entry = None
                    update(entry)
                else:   # current entry isn't end yet, pre-fill next
                    entry = self.next_entry
            else:   # too early -- fill current entry
                entry = self.entry
        if self.next_entry is None: # so we always know next
            self.next_entry = get()
        return self.entry_to_line(entry)

    def info(self):
        # fill first
        fill = self.fill()
        return {
            # 'fill': fill,
            'current': self.entry,
            'next': self.next_entry,
            'elapsed': time() - self.time,
        }

    def seek(self) -> float:
        return time() - self.time

    def all_entries(self) -> (str, str, str, str):
        for listpath in Config['lists']:
            with open(listpath, 'r', encoding='utf-8') as f:
                while line := f.readline():
                    segs = line.split(' ')
                    yield (segs[0], segs[1], segs[2], ' '.join(segs[3:]).strip())
        for dirpath in Config['directories']:
            for ogg in os.listdir(dirpath):
                yield self.path_to_entry(os.path.join(dirpath, ogg))
    
    def read_duration(self):
        with open(DURATION_CACHE, 'r', encoding='utf-8') as f:
            while line := f.readline():
                duration, path = line[:-1].split('\t')
                self.ogg_duration_map[path] = duration

    def save_duration(self, path, duration):
        with open(DURATION_CACHE, 'a', encoding='utf-8') as f:
            f.write('%s\t%s\n' % (duration, path))

filler = Filler()

class XonRadioHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if not self.security():
            self.send_response_only(400)
            self.end_headers()
            return
        host = self.headers.get('Host', DEF_HOST)
        urlbase = 'http://' + host
        def writes(s='', end='\n', encoding='utf-8'):
            self.wfile.write((s + end).encode(encoding))
        filler.use_host(host)
        if self.path == ROUTE_FILL:
            self.send_response_only(200)
            entry = filler.fill()
            data = entry.encode('utf-8')
            self.send_header('Content-Type', 'text/plain;charset=utf-8')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data)
            self.log('~fill', urllib.parse.unquote(entry))
            return
        elif self.path.startswith(ROUTE_PK3):
            url = self.path[len(ROUTE_PK3):-len('.pk3')]
            path = urllib.parse.unquote(url)
            if not os.path.isfile(path):
                self.send_response_only(404)
                self.end_headers()
                return
            self.send_response_only(200)
            buffer = io.BytesIO()
            with zipfile.ZipFile(buffer, 'w') as zbuffer:
                zbuffer.write(path, SONG_IN_PK3)
                zbuffer.writestr(INFO_IN_PK3, os.path.basename(path))
            size = buffer.seek(0, 2)
            buffer.seek(0)
            self.send_header('Content-Type', 'application/zip')
            self.send_header('Content-Length', str(size))
            self.end_headers()
            while chunk := buffer.read(BUFSIZE):
                self.wfile.write(chunk)
            self.log('~pk3', path)
            return
        elif self.path.startswith(ROUTE_OGG):
            url = self.path[len(ROUTE_OGG):]
            path = urllib.parse.unquote(url)
            if not os.path.isfile(path):
                self.send_response_only(404)
                self.end_headers()
                return
            self.send_response_only(200)
            size = os.stat(path).st_size
            self.send_header('Content-Type', 'application/zip')
            self.send_header('Content-Length', str(size))
            self.end_headers()
            with open(path, 'rb') as f:
                while chunk := f.read(BUFSIZE):
                    self.wfile.write(chunk)
            self.log('~ogg', path)
            return
        elif self.path.startswith(ROUTE_BROWSE):
            if not Config['allow_browse']:
                self.send_response_only(403)
                self.end_headers()
                return
            def fmttime(time: float):
                return '%02d:%02d' % (time // 60, time % 60 // 1)
            self.send_response_only(200)
            self.send_header('Content-Type', 'text/html;charset=utf-8')
            self.end_headers()
            # writes('<h1>Browse</h1>')
            if len(Config['directories']) > 0:
                writes('<h2>OGG</h2>')
                for oggdir in Config['directories']:
                    for oggfile in os.listdir(oggdir):
                        url = urlbase + ROUTE_OGG + os.path.join(oggdir, urllib.parse.quote(oggfile))
                        duration = fmttime(float(filler.duration_of(os.path.join(oggdir, oggfile))))
                        writes('<a href="%s">[ogg] (%s) %s</a><br>' % (url, duration, os.path.splitext(oggfile)[0]))
            if len(Config['lists']) > 0:
                writes('<h2>PK3</h2>')
                for listpath in Config['lists']:
                    with open(listpath, 'r', encoding='utf-8') as f:
                        for entry in f.readlines():
                            segs = entry.split(' ')
                            duration = fmttime(float(segs[2]))
                            writes('<a href="%s">[pk3] (%s) %s</a><br>' % (segs[0], duration, ' '.join(segs[3:])))
            if len(Config['directories']) + len(Config['lists']) == 0:
                writes('<p>Nothing is here</p>')
            self.log('~browse')
            return
        elif self.path.startswith(ROUTE_LIST):
            if not Config['allow_browse']:
                self.send_response_only(403)
                self.end_headers()
                return
            self.send_response_only(200)
            self.send_header('Content-Type', 'text/plain;charset=utf-8')
            self.end_headers()
            for entry in filler.all_entries():
                writes(filler.entry_to_line(entry))
            self.log('~list')
            return
        elif self.path.startswith(ROUTE_CFG):
            self.send_response_only(200)
            self.send_header('Content-Type', 'text/plain;charset=utf-8')
            self.end_headers()
            writes('set sv_radio 1')
            writes('set sv_radio_queue_autofill 1')
            writes('set sv_radio_queue_autofill_server "%s"' % (urlbase + ROUTE_FILL))
            self.log('~index')
            return
        elif self.path.startswith(ROUTE_ABOUT):
            self.send_response_only(200)
            self.send_header('Content-Type', 'text/json;charset=utf-8')
            self.end_headers()
            self.wfile.write(json.dumps(Config['about']).encode('utf-8'))
            self.log('~about')
            return
        elif self.path.startswith(ROUTE_INFO):
            self.send_response_only(200)
            self.send_header('Content-Type', 'text/json;charset=utf-8')
            self.end_headers()
            self.wfile.write(json.dumps(filler.info()).encode('utf-8'))
            self.log('~info')
            return
        else:
            if len(self.path) < 2:
                path = os.path.join(WEB_ROOT, 'index.html')
            else:
                path = os.path.join(WEB_ROOT, self.path[1:])
            if Config['web_interface'] and os.path.isfile(path):
                self.send_response_only(200)
                self.send_header('Content-Type', mime(path))
                self.send_header('Content-Length', os.stat(path).st_size)
                self.end_headers()
                with open(path, 'rb') as f:
                    while chunk := f.read(BUFSIZE):
                        self.wfile.write(chunk)
                self.log('~200', path)
            else:
                reply = 'Not Found'.encode('utf-8')
                self.send_response_only(404)
                self.send_header('Content-Type', 'text/plain;charset=utf-8')
                self.send_header('Content-Length', str(len(reply)))
                self.end_headers()
                self.wfile.write(reply)
                self.log('~404', path)
            return

    def security(self):
        if Config["forbid_non_xonotic"]:
            if self.headers.get('User-Agent', '').find('Xonotic') == -1:
                return False
        if '../' in self.path or '/..' in self.path:
            return False
        return True

    def log(self, *args, **kwargs):
        if Config['log']:
            client = self.client_address[0] + ':' + str(self.client_address[1])
            print(client, '<<', *args, **kwargs)


if __name__ == '__main__':
    host, port = Config['host'], Config['port']
    server = HTTPServer((host, port), XonRadioHandler)
    print('Serving at http://%s:%s'% (host or 'localhost', port))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
