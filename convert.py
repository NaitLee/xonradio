#!/usr/bin/env python3

import os

src = 'any-audio'
dst = 'ogg'
bitrate = '128k'

if not os.path.isdir(src):
    os.mkdir(src)
if not os.path.isdir(dst):
    os.mkdir(dst)

print(f"converting audio files from '{src}' to '{dst}' as ogg vorbis format")

for path in os.listdir(src):
    inpath = os.path.join(src, path)
    outpath = os.path.join(dst, os.path.splitext(path)[0] + '.ogg')
    if os.path.isfile(outpath):
        continue
    os.system(f"ffmpeg -y -i '{inpath}' -b:a {bitrate} '{outpath}'")

print('done')
